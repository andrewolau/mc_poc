-- email events
-- taken from Shanglin's code, except now we include all events
create or replace table `wx-bq-poc.personal.AL_MCPOC_event_store_view_email` as
		SELECT distinct 
			crn
			, time_utc
			, date
-- added
      , DATE_TRUNC(date, WEEK(MONDAY)) as wk_start_date
			, case when json_extract(attributes, "$.campaign_code[0]") like '%ENG%' then 'supermarkets' else banner end as banner
			, 'email' as channel
-- 			, case when upper(event_name) = 'EMAIL OPEN' then 'open' else 'clk' end as event
-- added all events
      , event_name
			, REPLACE(json_extract(attributes, "$.campaign_code[0]"), "\"", "") as campaign_code
			, CAST(REPLACE(json_extract(attributes, "$.campaign_or_control_start_date[0]"), "\"", "") AS DATE) as campaign_start_date
			, CAST(REPLACE(json_extract(attributes, "$.campaign_or_control_end_date[0]"), "\"", "") AS DATE) as campaign_end_date
			--, cast(null as string) as attributes
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events`
		where upper(channel) like '%EMAIL%'
-- removed filter
--       and upper(event_name) in ('EMAIL OPEN', 'EMAIL CLICK')
     ;
		
-- no activations for CAT-4031
select campaign_code, wk_start_date, event_name, count(*) as count from `wx-bq-poc.personal.AL_MCPOC_event_store_view_email`
where campaign_code in ("CAT-4031", "CAT-4105", "CVM-0007", "CVM-3509")
  and (DATE_TRUNC(date, WEEK(MONDAY)) = '2021-02-08'
--       OR DATE_TRUNC(date, WEEK(MONDAY)) = '2021-02-01'
        )
group by campaign_code, wk_start_date, event_name
order by campaign_code, wk_start_date, event_name;


-- no activations for CAT-4031
select * from (
  select
    campaign_code,
    channel,
    event,
    (DATE_TRUNC(cast(eventtime_utc as DATE), WEEK(MONDAY))) AS wk_start_date,
    count(*) as count from `wx-bq-poc.personal.AL_MCPOC_event_store_view`
  where campaign_code in ("CAT-4031", "CAT-4105", "CVM-0007", "CVM-3509")
  group by campaign_code,channel, wk_start_date, event
  order by campaign_code, channel,wk_start_date, event)
where wk_start_date = '2021-02-08';


