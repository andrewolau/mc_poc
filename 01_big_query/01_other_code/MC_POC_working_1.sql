DECLARE filter_campaigns ARRAY<STRING> DEFAULT ["CAT-4031", "CAT-4105", "CVM-0007", "CVM-3509"];
DECLARE filter_date DATE DEFAULT '2021-02-08';
DECLARE filter_channel ARRAY<STRING> DEFAULT ["rw_app", "email"];

-- NOTE: used Shangling's eventstore_view rather than the source eventstore_event to be 
-- consistent with his definition. His table doesn't have the activations/redemptions
-- for email I was using a view I created on eventstore_event anyways which didnt
-- make sense for activations/redemptions (some campaigns didnt have any)


CREATE OR REPLACE TABLE `wx-bq-poc.personal.AL_MCPOC_wk_events_0` as
-- Wilsons activations
SELECT
  crn,
  campaign_code,
  lower(campaign_channel) AS channel,
  "activation" as event,
  activation_datetime as eventtime_utc,
  fw_start_date AS wk_start_date 
FROM
  `wx-bq-poc.personal.AL_MCPOC_wilson_20210208`
WHERE
  activation_datetime is not null
  and if(filter_campaigns is not null, campaign_code IN UNNEST(filter_campaigns),true)
--   and if(filter_channel is not null, channel IN UNNEST(filter_channel),true)
  and if(filter_date is not null,  fw_start_date = filter_date,true)

UNION ALL

-- Wilsons redemptions
SELECT
  crn,
  campaign_code,
  lower(campaign_channel) AS channel,
  "redemption" as event,
  activation_datetime as eventtime_utc,
  fw_start_date AS wk_start_date 
FROM
  `wx-bq-poc.personal.AL_MCPOC_wilson_20210208`
WHERE
  redemption_datetime is not null
  and if(filter_campaigns is not null, campaign_code IN UNNEST(filter_campaigns),true)
--   and if(filter_channel is not null, channel IN UNNEST(filter_channel),true)
  and if(filter_date is not null,  fw_start_date = filter_date,true)

UNION ALL

-- other non activation/redemption touchpoints (open, click, ...)
select
  crn,
  campaign_code,
  channel,
  event,
  time_utc as eventtime_utc,
  (DATE_TRUNC(cast(time_utc as DATE), WEEK(MONDAY))) AS wk_start_date,
-- from `wx-bq-poc.personal.AL_MCPOC_event_store_view`
from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_eventstore_view_snapshot`
where
  if(filter_campaigns is not null, campaign_code IN UNNEST(filter_campaigns),true)
  and if(filter_channel is not null, channel IN UNNEST(filter_channel),true)
  and if(filter_date is not null,  (DATE_TRUNC(cast(time_utc as DATE), WEEK(MONDAY))) = filter_date,true)
; 

-- sort before dedup
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_wk_events_1` AS
select
  *
from `wx-bq-poc.personal.AL_MCPOC_wk_events_0`
order by crn, campaign_code, eventtime_utc
;

-- dedup so we only keep the first event of a campaign in a given week from a CRN
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_wk_events_2` AS
SELECT *, concat(campaign_code, "_", channel, "_", event) as event_node
  FROM (
    SELECT
        *,
        ROW_NUMBER()
            OVER (PARTITION BY crn, wk_start_date, campaign_code, event order by crn, wk_start_date, campaign_code, event, eventtime_utc)
            row_number
    FROM `wx-bq-poc.personal.AL_MCPOC_wk_events_1`
  )
WHERE row_number = 1 and event != "other"
order by crn, wk_start_date, eventtime_utc;

-- STEP 2: CREATING PATH
-- running activation subtotal for pivoting
-- each CRN will have many rows, one for each activation
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_working_01` AS
SELECT
  crn,
  wk_start_date,
  event_node,
  concat("event_",
  COUNT(*) OVER (PARTITION BY crn, wk_start_date
    ORDER BY
      crn,
      wk_start_date ROWS BETWEEN UNBOUNDED PRECEDING
      AND CURRENT ROW )) AS event_number_this_wk
FROM
  `wx-bq-poc.personal.AL_MCPOC_wk_events_2`
 ORDER BY
      crn,
      wk_start_date      
;

-- transposing rows to columns
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_working_02` AS
SELECT 
  crn,
  wk_start_date, 
    MAX(IF(event_number_this_wk = 'event_1' , concat( ' > ' ,event_node), '')) AS event_1,
    MAX(IF(event_number_this_wk = 'event_2' , concat( ' > ' ,event_node), '')) AS event_2,
    MAX(IF(event_number_this_wk = 'event_3' , concat( ' > ' ,event_node), '')) AS event_3,
    MAX(IF(event_number_this_wk = 'event_4' , concat( ' > ' ,event_node), '')) AS event_4,
    MAX(IF(event_number_this_wk = 'event_5' , concat( ' > ' ,event_node), '')) AS event_5,
    MAX(IF(event_number_this_wk = 'event_6' , concat( ' > ' ,event_node), '')) AS event_6,
    MAX(IF(event_number_this_wk = 'event_7' , concat( ' > ' ,event_node), '')) AS event_7,
    MAX(IF(event_number_this_wk = 'event_8' , concat( ' > ' ,event_node), '')) AS event_8,
    MAX(IF(event_number_this_wk = 'event_9' , concat( ' > ' ,event_node), '')) AS event_9,
    MAX(IF(event_number_this_wk = 'event_10' , concat( ' > ' ,event_node), '')) AS event_10,
    MAX(IF(event_number_this_wk = 'event_11' , concat( ' > ' ,event_node), '')) AS event_11,
    MAX(IF(event_number_this_wk = 'event_12' , concat( ' > ' ,event_node), '')) AS event_12,
    MAX(IF(event_number_this_wk = 'event_13' , concat( ' > ' ,event_node), '')) AS event_13,
    MAX(IF(event_number_this_wk = 'event_14' , concat( ' > ' ,event_node), '')) AS event_14,
    MAX(IF(event_number_this_wk = 'event_15' , concat( ' > ' ,event_node), '')) AS event_15,
    MAX(IF(event_number_this_wk = 'event_16' , concat( ' > ' ,event_node), '')) AS event_16,
    MAX(IF(event_number_this_wk = 'event_17' , concat( ' > ' ,event_node), '')) AS event_17,
    MAX(IF(event_number_this_wk = 'event_18' , concat( ' > ' ,event_node), '')) AS event_18,
    MAX(IF(event_number_this_wk = 'event_19' , concat( ' > ' ,event_node), '')) AS event_19,
    MAX(IF(event_number_this_wk = 'event_20' , concat( ' > ' ,event_node), '')) AS event_20
 
FROM `wx-bq-poc.personal.AL_MCPOC_working_01`
GROUP BY crn, wk_start_date 
ORDER BY crn, wk_start_date ;

-- creating path
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_working_03` AS
SELECT 
  crn, wk_start_date,
  concat("start",
    event_1,
    event_2,
    event_3,
    event_4,
    event_5,
    event_6,
    event_7,
    event_8,
    event_9,
    event_10,
    event_11,
    event_12,
    event_13,
    event_14,
    event_15,
    event_16,
    event_17,
    event_18,
    event_19,
    event_20
  ) as path
  
FROM `wx-bq-poc.personal.AL_MCPOC_working_02`
ORDER BY crn, wk_start_date ;
    
-- STEP 3: SPEND
-- table with spend by CRN and FW
-- filter for promo
-- ** NOTE: I've summed up this table by CRN and week, filtering for the campaigns selected
create or replace table `wx-bq-poc.personal.AL_MCPOC_inc_sale` as
select
  fw_start_date, crn, sum(inc_sales) as inc_sales
from
  `wx-bq-poc.Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current`
where
  if(filter_campaigns is not null, campaign_code IN UNNEST(filter_campaigns),true) and
  if(filter_date is not null,  fw_start_date = filter_date,true) and
  -- add promo
  pph = "promo"
group by
  fw_start_date, crn
;

-- STEP 4 JOIN SPEND
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_working_04` AS
SELECT 
  a.wk_start_date
  , a.crn   
  , b.inc_sales
  , path  
  , (case when b.inc_sales > 0 then concat(path, " > CONVERSION") else  concat(path, " > null") end) as Paths
  -- idea: more inc_sales worth >1 conversion
  , (case when b.inc_sales > 0 then 1 else 0 end) as Converted  -- conversion definition
  , cast((case when b.inc_sales > 0 then round(b.inc_sales, 0) else 0 end) as INT64) as Converted_weighted  -- conversion definition
  
 
FROM `wx-bq-poc.personal.AL_MCPOC_working_03` as a
left join `wx-bq-poc.personal.AL_MCPOC_inc_sale` as b
  on a.crn = b.crn and a.wk_start_date = b.fw_start_date
ORDER BY crn, wk_start_date ;

-- exporting to GCS storage
-- EXPORT DATA OPTIONS(
--   uri='gs://wx-personal/AndrewLau/nash/MC_POC/MCPOC_input_*.csv',
--   format='CSV',
--   overwrite=true,
--   header=true,
--   field_delimiter=';') AS
-- SELECT * FROM `wx-bq-poc.personal.AL_MCPOC_working_04`
