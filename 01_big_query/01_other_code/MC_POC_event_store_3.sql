drop table if exists `wx-bq-poc.personal.AL_MCPOC_event_store_user_journey`;
create table `wx-bq-poc.personal.AL_MCPOC_event_store_user_journey` as 
(
	with ws_conv_crn as 
	(
		SELECT distinct crn from `wx-bq-poc.personal.AL_MCPOC_event_store_activations` where campaign_code = 'CVM-1661'
	),
	
	base_ws as 
	(
		SELECT a.*
				, b.crn
				, b.date
				, b.time_utc
        , b.channel_event as channel
				, CONCAT(b.campaign_code, '_', b.channel_event) as channel_event
				, b.campaign_code as cc_event
				, d.conv_time
				, d.conv_flag
				, d.spend
				, lead(time_utc) over (partition by b.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by b.time_utc desc) as time_utc_pre
				, lead(CONCAT(b.campaign_code, '_', b.channel_event)) over (partition by b.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by b.time_utc desc) as channel_event_pre
				, case when d.conv_time is null then a.campaign_end_date else Date(d.conv_time, 'Australia/Sydney') end as end_date 
		FROM
		(
			(
				select distinct banner, campaign_code, campaign_type, campaign_start_date, campaign_end_date 
				from `wx-bq-poc.personal.AL_MCPOC_event_store_activations`
				where campaign_code <> 'CVM-1661'
			) a
			
			inner join 
			(
				SELECT distinct banner, campaign_code, crn, time_utc, date, channel_event 
				from `wx-bq-poc.personal.AL_MCPOC_event_store_events_staging`
			) b on a.banner = b.banner 
					and b.date BETWEEN a.campaign_start_date AND a.campaign_end_date
					and b.campaign_code = 'CVM-1661'
			
			left join
			(
				select crn, banner, campaign_code, campaign_start_date, campaign_end_date, min(time_utc) as conv_time, 1 as conv_flag, sum(spend) as spend
				from `wx-bq-poc.personal.AL_MCPOC_event_store_activations`
				group by 1,2,3,4,5
			) d on b.crn = d.crn 
					and a.banner = d.banner 
					and a.campaign_code = d.campaign_code 
					and a.campaign_start_date = d.campaign_start_date 
					and a.campaign_end_date = d.campaign_end_date
		)
	),
	
	base_act as 
	(
		SELECT a.*
				, b.crn
				, b.date
				, b.time_utc
        , b.channel_event as channel
				, CONCAT(b.campaign_code, '_', b.channel_event) as channel_event
				, b.campaign_code as cc_event
				, d.conv_time
				, d.conv_flag
				, d.spend
				, lead(time_utc) over (partition by b.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by b.time_utc desc) as time_utc_pre
				, lead(CONCAT(b.campaign_code, '_', b.channel_event)) over (partition by b.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by b.time_utc desc) as channel_event_pre
				, case when d.conv_time is null then a.campaign_end_date else Date(d.conv_time, 'Australia/Sydney') end as end_date 
		FROM
		(
			(
				select distinct banner, campaign_code, campaign_type, campaign_start_date, campaign_end_date 
				from `wx-bq-poc.personal.AL_MCPOC_event_store_activations`
			) a
			
			inner join 
			(
				SELECT distinct banner, campaign_code, crn, time_utc, date, channel_event 
				from `wx-bq-poc.personal.AL_MCPOC_event_store_events_staging`
			) b on a.banner = b.banner 
					and b.date BETWEEN a.campaign_start_date AND a.campaign_end_date
					and (SUBSTR(a.campaign_code,1,8) = SUBSTR(b.campaign_code,1,8)
						or
						b.campaign_code = '_AlwaysOn')
			
			left join
			(
				select crn, banner, campaign_code, campaign_start_date, campaign_end_date, min(time_utc) as conv_time, 1 as conv_flag, sum(spend) as spend
				from `wx-bq-poc.personal.AL_MCPOC_event_store_activations`
				group by 1,2,3,4,5
			) d on b.crn = d.crn 
					and a.banner = d.banner 
					and a.campaign_code = d.campaign_code 
					and a.campaign_start_date = d.campaign_start_date 
					and a.campaign_end_date = d.campaign_end_date
		)
	),
	
	base_onl as 
	(
		SELECT a.*
				, b.crn
				, b.date
				, b.time_utc
        , b.channel_event as channel
				, CONCAT(b.campaign_code, '_', b.channel_event) as channel_event
				, b.campaign_code as cc_event
				, c.conv_time
				, c.conv_flag
				, c.spend
				, lead(time_utc) over (partition by b.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by b.time_utc desc) as time_utc_pre
				, lead(CONCAT(b.campaign_code, '_', b.channel_event)) over (partition by b.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by b.time_utc desc) as channel_event_pre
				, case when c.conv_time is null then a.campaign_end_date else Date(c.conv_time, 'Australia/Sydney') end as end_date 
		FROM
		(
			(
				select 'supermarkets' as banner, 'ONLINE' as campaign_code, 'ONLINE' as campaign_type, min(date) as campaign_start_date, max(date) as campaign_end_date 
				from `wx-bq-poc.personal.AL_MCPOC_event_store_online_sales`
				group by 1
			) a
			
			inner join 
			(
				SELECT distinct banner, crn, time_utc, date, campaign_code, channel_event 
				from `wx-bq-poc.personal.AL_MCPOC_event_store_events_staging`
			) b on a.banner = b.banner 
					and b.date BETWEEN DATE_SUB(a.campaign_start_date, INTERVAL 6 DAY) AND a.campaign_end_date
			
			left join 
			(
				select crn, min(time_utc) as conv_time, 1 as conv_flag, sum(spend) as spend
				from `wx-bq-poc.personal.AL_MCPOC_event_store_online_sales`
				group by 1
			) c on b.crn = c.crn 
		)
		where ((c.conv_time is null and b.date between a.campaign_start_date AND a.campaign_end_date) 
				or (c.conv_time is not null and b.time_utc <= c.conv_time and b.date >= date_sub(Date(c.conv_time, "Australia/Sydney"), INTERVAL 6 DAY)))
	)
	
	SELECT 
  banner, 
  campaign_code, 
  campaign_type, 
  campaign_start_date, 
  campaign_end_date, 
  crn, 
  time_utc,
  --date,
  channel,
  channel_event,
  conv_time,
  case when conv_flag is null then 0 else conv_flag end as conv_flag, 
  spend
	from
	(
		select * 
		from base_ws
		where crn in (select * from ws_conv_crn)
			   and ((channel_event like '%clk%' and DATE_add(date, INTERVAL 7 DAY) >= end_date)
						or (channel_event not like '%clk%' and DATE_add(date, INTERVAL 1 DAY) >= end_date))
			   and (timestamp_diff(time_utc, time_utc_pre, SECOND) >= 600
				or time_utc_pre is null
				or channel_event_pre <> channel_event)
				
		union all 
		
		select * 
		from base_act 
		where ((channel_event not like '%AlwaysOn_%' and DATE_add(date, INTERVAL 7 DAY) >= end_date)
				or (channel_event like '%AlwaysOn_%' and DATE_add(date, INTERVAL 1 DAY) >= end_date and ((conv_flag is null and date <= end_date) or (conv_flag = 1 and time_utc < conv_time))))
			   and (timestamp_diff(time_utc, time_utc_pre, SECOND) >= 600
				or time_utc_pre is null
				or channel_event_pre <> channel_event)
		
		union all
		
		select * 
		from base_onl 
		where ((regexp_contains(channel_event, r'(?i)clk') and DATE_add(date, INTERVAL 7 DAY) >= end_date and ((conv_flag is null and date <= end_date) or (conv_flag = 1 and time_utc < conv_time)))
				or (not regexp_contains(channel_event, r'(?i)clk') and DATE_add(date, INTERVAL 1 DAY) >= end_date) and ((conv_flag is null and date <= end_date) or (conv_flag = 1 and time_utc < conv_time)))
			   and (timestamp_diff(time_utc, time_utc_pre, SECOND) >= 600
				or time_utc_pre is null
				or channel_event_pre <> channel_event)
	)	
);



select *
from `wx-bq-poc.personal.AL_MCPOC_event_store_user_journey`
--group by 1
limit 1000
;


select banner,campaign_type,campaign_code,count(*)
from `wx-bq-poc.personal.AL_MCPOC_event_store_user_journey`
group by 1,2,3
order by 2
limit 1000
;


select conv_flag,count(*)
from `wx-bq-poc.personal.AL_MCPOC_event_store_user_journey`
group by 1
;

