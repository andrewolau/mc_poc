-- See this for more info: https://bitbucket.org/andrewolau/mc_poc/wiki/Home
DECLARE filter_date DATE DEFAULT '2021-02-08';
DECLARE filter_supers BOOL DEFAULT true;
-- filter campaigns here
-- DECLARE filter_campaigns ARRAY<STRING> DEFAULT ["CAT-4031", "CAT-4105", "CVM-0007", "CVM-3509"];

-- STEP 0: FILTER WILSONS TABLE FOR SAFARI TOUCHPOINTS ONLY
CREATE OR REPLACE TABLE `wx-bq-poc.personal.AL_MCPOC_wilson_filter` as
SELECT
  b.*
FROM
-- safari matching inc sales output, filtered by date, promo
(SELECT DISTINCT campaign_code, campaign_start_date FROM Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current
  where if(filter_date is not null,  fw_start_date = filter_date, true) AND pph='promo') as a
left join `wx-bq-poc.personal.AL_MCPOC_wilson_20210208` as b
  on a.campaign_code = b.campaign_code and cast(a.campaign_start_date as DATE) = b.campaign_start_date
where
  if(filter_date is not null,  b.fw_start_date = filter_date,true)
  and if(filter_supers is not null,  b.campaign_division_supers = filter_supers,true)
--   and if(filter_campaigns is not null, b.campaign_code IN UNNEST(filter_campaigns),true)

;

-- table with info of campaigns considered
create or replace table `wx-bq-poc.personal.AL_MCPOC_campaign_info` as
SELECT
*
FROM
  `wx-bq-poc.loyalty.campaign`
where
  cmpgn_code IN (select distinct campaign_code from `wx-bq-poc.personal.AL_MCPOC_wilson_filter`)
;

-- exporting to GCS storage
EXPORT DATA OPTIONS(
  uri='gs://wx-personal/AndrewLau/nash/MC_POC/campaign_info*.csv',
  format='CSV',
  overwrite=true,
  header=true
  ) AS
SELECT * FROM `wx-bq-poc.personal.AL_MCPOC_campaign_info`;

-- check no non supers campaigns in safari data
ASSERT (
(select count(*) from `wx-bq-poc.personal.AL_MCPOC_campaign_info` where business_unit != "supermarkets") = 0
) AS 'Check failed... non supers campaigns are in the safari data';


-- STEP 1: COMBINING ALL TOUCHPOINTS
CREATE OR REPLACE TABLE `wx-bq-poc.personal.AL_MCPOC_wk_events_0` as
-- Wilsons activations
SELECT DISTINCT
  crn,
  campaign_code,
  lower(campaign_channel) AS channel,
--   concat("wk_", campaign_week_nbr) as campaign_week_nbr,
  CAST(campaign_start_date AS STRING) as campaign_start_date,
  "activation" as event,
  activation_datetime as eventtime_utc,
  fw_start_date AS wk_start_date 
FROM
  `wx-bq-poc.personal.AL_MCPOC_wilson_filter`
WHERE
  activation_datetime is not null
  
UNION ALL

-- Wilsons redemptions
SELECT DISTINCT
  crn,
  campaign_code,
  lower(campaign_channel) AS channel,
--   concat("wk_", campaign_week_nbr) as campaign_week_nbr,
  CAST(campaign_start_date AS STRING) as campaign_start_date,
  "redemption" as event,
  redemption_datetime as eventtime_utc,
  fw_start_date AS wk_start_date 
FROM
  `wx-bq-poc.personal.AL_MCPOC_wilson_filter`
WHERE
  redemption_datetime is not null

UNION ALL

-- Wilsons purchases (activated only, e.g. purchase time stamp is after activation time stamp)
SELECT DISTINCT
  crn,
  campaign_code,
  lower(campaign_channel) AS channel,
--   concat("wk_", campaign_week_nbr) as campaign_week_nbr,
  CAST(campaign_start_date AS STRING) as campaign_start_date,
  "activated_purchase" as event,
  purchase_datetime as eventtime_utc,
  fw_start_date AS wk_start_date 
FROM
  `wx-bq-poc.personal.AL_MCPOC_wilson_filter`
WHERE
  purchase_datetime is not null and  -- there is a purchase
    (
      (activation_datetime is not null and purchase_datetime >= activation_datetime)  -- purchase happened after activation
    )

UNION ALL

-- Wilsons purchases (unactivated purchases only)
SELECT DISTINCT
  crn,
  campaign_code,
  lower(campaign_channel) AS channel,
--   concat("wk_", campaign_week_nbr) as campaign_week_nbr,
  CAST(campaign_start_date AS STRING) as campaign_start_date,
  "unactivated_purchase" as event,
  purchase_datetime as eventtime_utc,
  fw_start_date AS wk_start_date 
FROM
  `wx-bq-poc.personal.AL_MCPOC_wilson_filter`
WHERE
  purchase_datetime is not null and  -- there is a purchase
    (
      (activation_datetime is not null and purchase_datetime < activation_datetime)  -- purchase happened before activation
      or (activation_datetime is  null)  -- purchase with no activation
    )
;

-- sort before dedup
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_wk_events_1` AS
select
  *
from `wx-bq-poc.personal.AL_MCPOC_wk_events_0`
order by crn, campaign_code, campaign_start_date, eventtime_utc
;

-- dedup so we only keep the first event of a campaign in a given week from a CRN
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_wk_events_2` AS
SELECT *,
--   concat(campaign_code, "_", channel, "_", event) as event_node
  concat(campaign_code, "_", campaign_start_date, "_", event) as event_node,
  (case
    when event = "activation" then 3
    when event = "redemption" then 2
    when event = "activated_purchase" then 1
    when event = "unactivated_purchase" then 4
    else 9
    end  
  ) as weight
  FROM (
    SELECT
        *,
        ROW_NUMBER()
            OVER (PARTITION BY crn, wk_start_date, campaign_code, campaign_start_date, event order by crn, wk_start_date, campaign_code, campaign_start_date, event, eventtime_utc)
            row_number
    FROM `wx-bq-poc.personal.AL_MCPOC_wk_events_1`
  )
WHERE row_number = 1
-- order by crn, wk_start_date, eventtime_utc
;

-- check deduping (first event per campaign per week per crn)
ASSERT
(select count(*) from `wx-bq-poc.personal.AL_MCPOC_wk_events_2`) = (select count(*) from (select distinct crn, wk_start_date, campaign_code, campaign_start_date, event from `wx-bq-poc.personal.AL_MCPOC_wk_events_1`))
AS 'Dedup (first event per campaign) check failed';

-- check weights
ASSERT 
((select count(*) from `wx-bq-poc.personal.AL_MCPOC_wk_events_2` where weight = 9) = 0)
AS 'Error... not all weights assigned';

-- STEP 2: CREATING PATH
-- running activation subtotal for pivoting
-- each CRN will have many rows, one for each activation
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_working_01` AS
SELECT
  crn,
  wk_start_date,
  event_node,
  eventtime_utc,
  concat("event_",
  COUNT(*) OVER (PARTITION BY crn, wk_start_date
    ORDER BY
      crn,
      wk_start_date, eventtime_utc
      -- need tie breakers for when events happen at the same time
      -- weight gives importance of events
      , weight, campaign_start_date, campaign_code, channel
      
      ROWS BETWEEN UNBOUNDED PRECEDING
      AND CURRENT ROW )) AS event_number_this_wk
FROM
  `wx-bq-poc.personal.AL_MCPOC_wk_events_2`
 ORDER BY
      crn,
      wk_start_date      
;

-- max touchpoints for a crn so far is 31. have set to 50 max for now. unlikely to be needed, and will
-- have an immaterial impact on the MC model, as the number of CRNs that > 50 touchpoints will be so small
-- select distinct event_number_this_wk from `wx-bq-poc.personal.AL_MCPOC_working_01`;
-- transposing rows to columns
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_working_02` AS
SELECT 
  crn,
  wk_start_date, 
    MAX(IF(event_number_this_wk = 'event_1' , concat( '' ,event_node), '')) AS event_1,
    MAX(IF(event_number_this_wk = 'event_2' , concat( ' > ' ,event_node), '')) AS event_2,
    MAX(IF(event_number_this_wk = 'event_3' , concat( ' > ' ,event_node), '')) AS event_3,
    MAX(IF(event_number_this_wk = 'event_4' , concat( ' > ' ,event_node), '')) AS event_4,
    MAX(IF(event_number_this_wk = 'event_5' , concat( ' > ' ,event_node), '')) AS event_5,
    MAX(IF(event_number_this_wk = 'event_6' , concat( ' > ' ,event_node), '')) AS event_6,
    MAX(IF(event_number_this_wk = 'event_7' , concat( ' > ' ,event_node), '')) AS event_7,
    MAX(IF(event_number_this_wk = 'event_8' , concat( ' > ' ,event_node), '')) AS event_8,
    MAX(IF(event_number_this_wk = 'event_9' , concat( ' > ' ,event_node), '')) AS event_9,
    MAX(IF(event_number_this_wk = 'event_10' , concat( ' > ' ,event_node), '')) AS event_10,
    MAX(IF(event_number_this_wk = 'event_11' , concat( ' > ' ,event_node), '')) AS event_11,
    MAX(IF(event_number_this_wk = 'event_12' , concat( ' > ' ,event_node), '')) AS event_12,
    MAX(IF(event_number_this_wk = 'event_13' , concat( ' > ' ,event_node), '')) AS event_13,
    MAX(IF(event_number_this_wk = 'event_14' , concat( ' > ' ,event_node), '')) AS event_14,
    MAX(IF(event_number_this_wk = 'event_15' , concat( ' > ' ,event_node), '')) AS event_15,
    MAX(IF(event_number_this_wk = 'event_16' , concat( ' > ' ,event_node), '')) AS event_16,
    MAX(IF(event_number_this_wk = 'event_17' , concat( ' > ' ,event_node), '')) AS event_17,
    MAX(IF(event_number_this_wk = 'event_18' , concat( ' > ' ,event_node), '')) AS event_18,
    MAX(IF(event_number_this_wk = 'event_19' , concat( ' > ' ,event_node), '')) AS event_19,
    MAX(IF(event_number_this_wk = 'event_20' , concat( ' > ' ,event_node), '')) AS event_20,
    MAX(IF(event_number_this_wk = 'event_21' , concat( ' > ' ,event_node), '')) AS event_21,
    MAX(IF(event_number_this_wk = 'event_22' , concat( ' > ' ,event_node), '')) AS event_22,
    MAX(IF(event_number_this_wk = 'event_23' , concat( ' > ' ,event_node), '')) AS event_23,
    MAX(IF(event_number_this_wk = 'event_24' , concat( ' > ' ,event_node), '')) AS event_24,
    MAX(IF(event_number_this_wk = 'event_25' , concat( ' > ' ,event_node), '')) AS event_25,
    MAX(IF(event_number_this_wk = 'event_26' , concat( ' > ' ,event_node), '')) AS event_26,
    MAX(IF(event_number_this_wk = 'event_27' , concat( ' > ' ,event_node), '')) AS event_27,
    MAX(IF(event_number_this_wk = 'event_28' , concat( ' > ' ,event_node), '')) AS event_28,
    MAX(IF(event_number_this_wk = 'event_29' , concat( ' > ' ,event_node), '')) AS event_29,
    MAX(IF(event_number_this_wk = 'event_30' , concat( ' > ' ,event_node), '')) AS event_30,
    MAX(IF(event_number_this_wk = 'event_31' , concat( ' > ' ,event_node), '')) AS event_31,
    MAX(IF(event_number_this_wk = 'event_32' , concat( ' > ' ,event_node), '')) AS event_32,
    MAX(IF(event_number_this_wk = 'event_33' , concat( ' > ' ,event_node), '')) AS event_33,
    MAX(IF(event_number_this_wk = 'event_34' , concat( ' > ' ,event_node), '')) AS event_34,
    MAX(IF(event_number_this_wk = 'event_35' , concat( ' > ' ,event_node), '')) AS event_35,
    MAX(IF(event_number_this_wk = 'event_36' , concat( ' > ' ,event_node), '')) AS event_36,
    MAX(IF(event_number_this_wk = 'event_37' , concat( ' > ' ,event_node), '')) AS event_37,
    MAX(IF(event_number_this_wk = 'event_38' , concat( ' > ' ,event_node), '')) AS event_38,
    MAX(IF(event_number_this_wk = 'event_39' , concat( ' > ' ,event_node), '')) AS event_39,
    MAX(IF(event_number_this_wk = 'event_40' , concat( ' > ' ,event_node), '')) AS event_40,
    MAX(IF(event_number_this_wk = 'event_41' , concat( ' > ' ,event_node), '')) AS event_41,
    MAX(IF(event_number_this_wk = 'event_42' , concat( ' > ' ,event_node), '')) AS event_42,
    MAX(IF(event_number_this_wk = 'event_43' , concat( ' > ' ,event_node), '')) AS event_43,
    MAX(IF(event_number_this_wk = 'event_44' , concat( ' > ' ,event_node), '')) AS event_44,
    MAX(IF(event_number_this_wk = 'event_45' , concat( ' > ' ,event_node), '')) AS event_45,
    MAX(IF(event_number_this_wk = 'event_46' , concat( ' > ' ,event_node), '')) AS event_46,
    MAX(IF(event_number_this_wk = 'event_47' , concat( ' > ' ,event_node), '')) AS event_47,
    MAX(IF(event_number_this_wk = 'event_48' , concat( ' > ' ,event_node), '')) AS event_48,
    MAX(IF(event_number_this_wk = 'event_49' , concat( ' > ' ,event_node), '')) AS event_49,
    MAX(IF(event_number_this_wk = 'event_50' , concat( ' > ' ,event_node), '')) AS event_50 
FROM `wx-bq-poc.personal.AL_MCPOC_working_01`
GROUP BY crn, wk_start_date 
ORDER BY crn, wk_start_date ;

-- check uniqueness of CRNs... OK
ASSERT
(select count(1) from (select distinct crn from `wx-bq-poc.personal.AL_MCPOC_working_02`)) = (select count(1) from `wx-bq-poc.personal.AL_MCPOC_working_02`)
AS 'Uniqueness of CRNs... check failed';
 
-- creating path
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_working_03` AS
SELECT 
  crn, wk_start_date,
  concat(
    event_1,
    event_2,
    event_3,
    event_4,
    event_5,
    event_6,
    event_7,
    event_8,
    event_9,
    event_10,
    event_11,
    event_12,
    event_13,
    event_14,
    event_15,
    event_16,
    event_17,
    event_18,
    event_19,
    event_20,
    event_21,
    event_22,
    event_23,
    event_24,
    event_25,
    event_26,
    event_27,
    event_28,
    event_29,
    event_30,
    event_31,
    event_32,
    event_33,
    event_34,
    event_35,
    event_36,
    event_37,
    event_38,
    event_39,
    event_40,
    event_41,
    event_42,
    event_43,
    event_44,
    event_45,
    event_46,
    event_47,
    event_48,
    event_49,
    event_50
  ) as path
  
FROM `wx-bq-poc.personal.AL_MCPOC_working_02`
ORDER BY crn, wk_start_date ;
    
-- STEP 3: SPEND
-- table with spend by CRN and FW
-- filter for promo
-- ** NOTE: I've summed up this table by CRN and week, filtering for the campaigns selected
create or replace table `wx-bq-poc.personal.AL_MCPOC_inc_sale` as
select
  fw_start_date, crn, sum(inc_sales) as inc_sales
from
  `wx-bq-poc.Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current`
where
--   if(filter_campaigns is not null, campaign_code IN UNNEST(filter_campaigns),true) and
  if(filter_date is not null,  fw_start_date = filter_date,true) and
  pph = "promo"
group by
  fw_start_date, crn
;

-- summary of safari results for comparison to MC POC
create or replace table `wx-bq-poc.personal.AL_MCPOC_safari_campaign_inc_sales` as
SELECT
  fw_start_date,
  pph,
  campaign_code,
  campaign_start_date,
  COUNT(*) AS num_CRN,
  SUM(inc_sales) AS inc_sales
FROM
  Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current
WHERE
IF
  (filter_date IS NOT NULL,
    fw_start_date = filter_date,
    TRUE)
  AND pph='promo'
GROUP BY
  1,
  2,
  3,
  4
ORDER BY
  1,
  3,
  4 ;
  
-- exporting to GCS storage
EXPORT DATA OPTIONS(
  uri='gs://wx-personal/AndrewLau/nash/MC_POC/safari_campaign_inc_sales*.csv',
  format='CSV',
  overwrite=true,
  header=true)
  AS
SELECT * FROM `wx-bq-poc.personal.AL_MCPOC_safari_campaign_inc_sales`
;


-- STEP 4 JOIN SPEND
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_working_04` AS
SELECT 
  a.wk_start_date
  , a.crn   
  , b.inc_sales
  , a.path  
--   , (case when b.inc_sales > 0 then concat("start > ", path, " > CONVERSION") else  concat("start > ", path, " > null") end) as Paths
  -- idea: more inc_sales worth >1 conversion
  , (case when b.inc_sales > 0 then 1 else 0 end) as conversion_inc_sales_positive
  , (case when b.inc_sales > 0 then 0 else 1 end) as non_conversion_inc_sales_positive
  , (case when a.path LIKE "%activated_purchase%" then 1 else 0 end) as conversion_act_purchase    
  , (case when a.path LIKE "%activated_purchase%" then 0 else 1 end) as non_conversion_act_purchase 
 
FROM `wx-bq-poc.personal.AL_MCPOC_working_03` as a
left join `wx-bq-poc.personal.AL_MCPOC_inc_sale` as b
  on a.crn = b.crn and a.wk_start_date = b.fw_start_date
ORDER BY crn, wk_start_date ;

-- check what percentage of inc sales was able to be left joined onto the customer journey
ASSERT (
(select
  abs(a.inc_sales / b.inc_sales - 1) as abs_diff
from
  (select 1 as id, sum(inc_sales) as inc_sales from `wx-bq-poc.personal.AL_MCPOC_inc_sale`) as a
inner join 
  (select 1 as id, sum(inc_sales) as inc_sales from `wx-bq-poc.personal.AL_MCPOC_working_04`) as b
  on a.id = b.id) < 0.025
) AS 'Check failed: % of inc sales left unjoined onto customer journey is greater than 2.5%';

-- STEP 5 CLEAN UP INTO CA FORMAT
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_MCPOC_working_05_ca_format` AS
SELECT 
  wk_start_date
  , path  
  , count(crn) as num_crn
  , round(sum(inc_sales), 4) as inc_sales
  , sum(conversion_inc_sales_positive) as conversion_inc_sales_positive
  , sum(non_conversion_inc_sales_positive) as non_conversion_inc_sales_positive
  , sum(conversion_act_purchase) as conversion_act_purchase
  , sum(non_conversion_act_purchase) as non_conversion_act_purchase
FROM `wx-bq-poc.personal.AL_MCPOC_working_04` as a
GROUP BY
  wk_start_date
  , path  
ORDER BY
wk_start_date, path
;

-- compare against backup
ASSERT (
(select count(*) from (
  (SELECT * FROM `wx-bq-poc.personal.AL_MCPOC_working_05_ca_format`
    EXCEPT DISTINCT
    SELECT * from `wx-bq-poc.personal.AL_MCPOC_tmp_working_05_ca_format`)
  UNION ALL
  (SELECT * FROM `wx-bq-poc.personal.AL_MCPOC_tmp_working_05_ca_format`
    EXCEPT DISTINCT
    SELECT * from `wx-bq-poc.personal.AL_MCPOC_working_05_ca_format`)
)) = 0
) AS 'Check failed: different to final CA output to older run is';

-- exporting to GCS storage
EXPORT DATA OPTIONS(
  uri='gs://wx-personal/AndrewLau/nash/MC_POC/MCPOC_input_*.csv',
  format='CSV',
  overwrite=true,
  header=true)
  AS
SELECT * FROM `wx-bq-poc.personal.AL_MCPOC_working_05_ca_format`;

